import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import Select, WebDriverWait
from pyvirtualdisplay import Display
import re
from driver2 import loadpath as driver2
import csv

def get_link(page_text=""):
    url = re.search("(?P<url>https?://[^\s]+)", page_text).group("url")
    if url.endswith("')"):
        return url[:-2]
    return url


def get_profile(elements=[]):
    if elements is None or elements.__len__() == 0:
        return {}

    profile = {

    }

    for _elem in elements:
        field_label = _elem.find_element_by_css_selector(".fieldLabel")
        field_body = _elem.find_element_by_css_selector(".fieldBody")

        label = ""
        body = ""
        if field_label is not None:
            label = field_label.text.strip()

        if field_body is not None:
            body = field_body.text.strip()

        if label in ["First name", "Last name", "Organization", "Email", "Email 2", "Phone", "Street address", "City",
                     "State", "Zip", ]:
            profile[label] = body

    return profile


def browse():

    with Display(visible=0, size=[320,240]) as display:
        url = 'http://www.smba.net/directory'
        chrome_option = Options()
        chrome_browser = webdriver.Chrome(executable_path=driver2.load_path(), chrome_options=chrome_option)
        chrome_browser.get(url=url)
        WebDriverWait(chrome_browser, 15).until(lambda _chrome_browser: chrome_browser.find_elements_by_css_selector("#idPagingData > select > option"))

        profile_list = []
        headers = {}

        values = []
        for option_dept in chrome_browser.find_elements_by_css_selector("#idPagingData > select > option"):
            value = option_dept.get_attribute("value")
            values.append( value )

        for value in values:
            select = Select(chrome_browser.find_element_by_css_selector('#idPagingData > select'))
            select.select_by_value( value )

            print 'running for -> ',value

            WebDriverWait(chrome_browser, 5).until(lambda _chrome_browser: chrome_browser.find_elements_by_css_selector("#membersTable > tbody > tr"))
            elements = chrome_browser.find_elements_by_css_selector("#membersTable > tbody > tr")
            record = 0
            for elem in elements:
                print 'storing record -> ',record
                txt = elem.get_attribute("onclick")
                page_link = get_link(txt)

                chrome_browser.execute_script('''window.open("about:blank", "_blank");''')
                chrome_browser.switch_to.window( chrome_browser.window_handles[1] )
                chrome_browser.get(page_link)
                inner_elements = chrome_browser.find_elements_by_css_selector(".sectionContainer .fieldSubContainer")
                profile = get_profile(inner_elements)
                profile_list.append( profile)
                for key in profile.keys():
                    headers[key]=1

                chrome_browser.close()
                chrome_browser.switch_to.window(chrome_browser.window_handles[0])
                record+=1


    with open("SantaMonicaBarAssociation.csv", 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=headers.keys())
        writer.writeheader()

        for each_profile in profile_list:
            tmp_profile = {}
            for key in headers.keys():
                tmp_profile[key] = each_profile.get(key, "None")

            print each_profile
            writer.writerow(tmp_profile)


browse()
