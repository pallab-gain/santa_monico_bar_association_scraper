# README #

A python selenium based web scraper that scrape all the bar members profile from 
[http://www.smba.net/directory](http://www.smba.net/directory), and store them in csv format.

### What is this repository for? ###

* To scrape bar member profiles from [http://www.smba.net/directory](http://www.smba.net/directory). 

### How do I get set up? ###

* Python 2.7
* [requirements.txt](src/master/requirements.txt)

### Who do I talk to? ###

* [pallab-gain](https://bitbucket.org/pallab-gain)